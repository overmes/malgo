import requests

import pymongo
import sys
from pymongo.mongo_client import MongoClient


client = MongoClient()
db = client.users
col = db.users

chunk = 10000
start_index = 0 if len(sys.argv) < 2 else int(sys.argv[1])/chunk
for i in range(start_index, 3195632/chunk + 1):
    start = chunk * i
    end = chunk * (i + 1)
    print start, end

    data_list = []
    for doc in col.find().sort([('_id', pymongo.ASCENDING)]).skip(start).limit(chunk):
        data_list.append({
                "Name": doc['username'],
                "Id": doc['_id']
            })

    # print data_list
    r = requests.post('http://cc-overmes.rhcloud.com/api/user/', json={'Users': data_list})
    print r.status_code, r.content
