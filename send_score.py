import requests

import pymongo
from pymongo.mongo_client import MongoClient
import sys


def scores_to_list(scores):
    anime = []
    manga = []
    for key, (score, status, last) in scores.items():
        int_id = int(key)
        if int_id > 0:
            anime.append({'Status': status, 'Id': int_id, 'Score': score, 'LastUpdate': last})
        else:
            manga.append({'Status': status, 'Id': abs(int_id), 'Score': score, 'LastUpdate': last})
    return anime, manga


client = MongoClient()
db = client.users
col = db.users


def send_data(data_list):
    # print data_list
    r = requests.post(servers[current_server_index], json={'Scores': data_list})
    print r.status_code, r.content


servers = {
    1: 'http://worker1-hanayamata.rhcloud.com/api/score/',
    2: 'http://worker2-hanayamata.rhcloud.com/api/score/',
    3: 'http://worker3-hanayamata.rhcloud.com/api/score/',
    4: 'http://worker4-overmes.rhcloud.com/api/score/',
    5: 'http://worker5-overmes.rhcloud.com/api/score/'
}
current_server_index = int(sys.argv[1])
print servers[current_server_index]

chunk = 5000
start_index = 0 if len(sys.argv) < 3 else int(sys.argv[2]) / chunk
for i in range(start_index, 3195632 / chunk + 1):
    start = chunk * i
    end = chunk * (i + 1)
    print start, end

    data_list = []
    for doc in col.find().sort([('_id', pymongo.ASCENDING)]).skip(start).limit(chunk):
        if doc['_id'] % 5 == current_server_index - 1:
            anime, manga = scores_to_list(doc['scores'])
            data_list.append({
                "UserName": doc['username'],
                "AnimeList": anime,
                "MangaList": manga,
                "UserId": doc['_id']
            })
    try:
        send_data(data_list)
    except Exception as e:
        print e
        send_data(data_list)
