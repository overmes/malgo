package control

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
)

type UserMessage struct {
	Users []UserName
}

func serveUser(w http.ResponseWriter, r *http.Request) {
	var res interface{}
	var err error

	c := controlServer.Session.DB(DATABASE).C(USERNAMECOLLECTION)
	switch r.Method {
	case "GET":
		get := r.URL.Query()
		offset, offsetOk := get["offset"]
		limit, limitOk := get["limit"]

		if !offsetOk || !limitOk {
			err = errors.New("Wrong data")
			break
		}
		limitInt, limitErr := strconv.Atoi(limit[0])
		offsetInt, offsetErr := strconv.Atoi(offset[0])
		if limitErr != nil || offsetErr != nil {
			err = errors.New(fmt.Sprint(limitErr, offsetErr))
			break
		}

		var users []UserName
		err = c.Find(nil).Skip(offsetInt).Limit(limitInt).All(&users)
		res = users
	case "POST":
		body, err := ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err == nil {
			data := UserMessage{}
			err = json.Unmarshal(body, &data)
			if err == nil {
				for i := range data.Users {
					err = c.Insert(&data.Users[i])
				}
				res = len(data.Users)
			}
		}

	}

	statusCode := http.StatusOK
	if err != nil {
		statusCode = http.StatusInternalServerError
		fmt.Println(err)
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(statusCode)
	err = json.NewEncoder(w).Encode(FormatResponse(res, err))
	if err != nil {
		fmt.Println(err)
	}
}
