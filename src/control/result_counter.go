package control

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"malpar"
	"net/http"
	"sort"
	"time"
)

type ResultCounter struct {
	Servers []string
}

type PearsonRequest struct {
	UserList malpar.UserList
	Anime    bool
	Manga    bool
	Share    int
}

func NewResultCounter(servers []string) *ResultCounter {
	return &ResultCounter{servers}
}

func (r *ResultCounter) Start() {
	for {
		var results []Result
		results, err := FilterResult(bson.M{"status": PENDING}, "created", 10)
		// TODO: save username/id in db
		if err == nil {
			for i := range results {
				fmt.Println("Start task ", results[i])
				results[i].Status = COUNTING
				results[i].Update()
				go r.GetPearsonResults(results[i])
			}
		} else {
			fmt.Println(err)
		}

		time.Sleep(1 * time.Second)
	}
}

func (r *ResultCounter) GetPearsonResults(result Result) {
	fmt.Println("Get scores for ", result.UserName)
	userList, err := malpar.GetUserScoresByName(result.UserName, 3)

	if err != nil {
		result.Status = ERROR
		result.Progress = 100
		result.Errors = append(result.Errors, "Can't take user scores "+err.Error())
		err = result.Update()
		fmt.Printf("Save result %v\n", result.Id)
		if err != nil {
			fmt.Println(err)
		}
	} else {
		request := PearsonRequest{userList, result.Anime, result.Manga, result.Share}
		dataChan := make(chan ResponseMessage, len(r.Servers))
		errChan := make(chan error, len(r.Servers))
		for i := range r.Servers {
			// TODO: shuffle
			go r.asyncWorkerRequest(r.Servers[i], request, dataChan, errChan)
		}
		for i := range r.Servers {
			select {
			case err := <-errChan:
				result.Errors = append(result.Errors, err.Error())
				fmt.Println("Worker Request error", err)
			case data := <-dataChan:
				result.Pearson = append(result.Pearson, data.Data.Pearson...)
				result.Compare += data.Data.Users
				sort.Sort(sort.Reverse(result.Pearson))
				if len(result.Pearson) > 100 {
					result.Pearson = result.Pearson[:100]
				}
			}

			result.Progress += 100 / len(r.Servers)
			if i == len(r.Servers)-1 {
				result.Status = COMPLITED
				result.Completed = time.Now()
				result.Progress = 100
			}
			err = result.Update()
			fmt.Printf("Save result %v\n", result.Id)
			if err != nil {
				fmt.Println("Save result error " + err.Error())
			}
		}
	}

}

func (r *ResultCounter) asyncWorkerRequest(url string, request PearsonRequest, dataChan chan ResponseMessage, errChan chan error) {
	fmt.Println("Send request ", url)
	res, err := r.MakeWorkerQuery(url, request)
	if err != nil {
		errChan <- err
	} else {
		dataChan <- res
	}
}

type ResponseData struct {
	Pearson []malpar.PearsonResult
	Users   int
}

type ResponseMessage struct {
	Status  string
	Message string
	Data    ResponseData
}

func (r *ResultCounter) MakeWorkerQuery(url string, request PearsonRequest) (ResponseMessage, error) {
	client := http.Client{Timeout: 90 * time.Second}
	data := ResponseMessage{}

	postBody, err := json.Marshal(request)
	resp, err := client.Post(url, "applicatin/json", bytes.NewBuffer(postBody))
	if err != nil {
		return data, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return data, errors.New("Status code: " + string(resp.StatusCode))
	}
	var body []byte
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return data, err
	}
	err = json.Unmarshal(body, &data)
	if err != nil {
		return data, err
	}
	if data.Status != "ok" {
		return data, errors.New(data.Message)
	}
	fmt.Println("Received ", data)
	return data, nil
}
