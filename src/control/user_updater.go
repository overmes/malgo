package control

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"malpar"
	"time"
)

const (
	CHECKUSERRANGE = 100
)

func StartUserUpdater() {
	go FullUpdater()
	go PersonalDataUpdate()
	counter := 0
	for {
		go GetNewUsers()
		counter++
		time.Sleep(time.Hour)
	}
}

type FullUpdateId struct {
	Id     int `bson:"_id"`
	LastId int
}

func (f *FullUpdateId) Save() {
	c := controlServer.Session.DB(DATABASE).C("fullupdate")
	c.Upsert(bson.M{"_id": f.Id}, f)
}

func (f *FullUpdateId) FromDb() {
	c := controlServer.Session.DB(DATABASE).C("fullupdate")
	c.Find(bson.M{"_id": f.Id}).One(f)
}

func FullUpdater() {
	for {
		FullUpdate()
		lastFullUpdate := FullUpdateId{0, 0}
		lastFullUpdate.Save()
		time.Sleep(30 * 24 * time.Hour)
	}
}

func FullUpdate() {
	lastFullUpdate := FullUpdateId{0, 0}
	lastFullUpdate.FromDb()

	logs := make([]string, 0)
	errorsLog := make([]string, 0)

	lastUser, err := GetUserName(bson.M{}, "-_id")
	if err != nil {
		fmt.Println("Can't find last user ", err)
	}

	for i := lastFullUpdate.LastId; i < lastUser.Id; i++ {
		username, err := malpar.GetUserNameById(i, 3)
		if err != nil {
			if err.Error() == "User not exist" {
				user := UserName{i, username, time.Now()}
				err = user.Delete()
				if err != nil && err.Error() != "not found" {
					errorsLog = append(errorsLog, err.Error())
				}
			} else {
				errorsLog = append(errorsLog, err.Error())
			}
		} else {
			logs = append(logs, fmt.Sprintf("%v=%v ", i, username))
			user := UserName{i, username, time.Now()}
			err = user.Save()
			if err != nil {
				errorsLog = append(errorsLog, err.Error())
			}
			lastFullUpdate.LastId = i
			lastFullUpdate.Save()
		}

		if len(logs) > 100 {
			fmt.Println("User added ", logs)
			logs = make([]string, 0)
		}
		if len(errorsLog) > 100 {
			fmt.Println("User update errors ", errorsLog)
			errorsLog = make([]string, 0)
		}
	}
}

func PersonalDataUpdate() {
	lastFullUpdate := FullUpdateId{1, 0}
	lastFullUpdate.FromDb()

	logs := make([]string, 0)
	errorsLog := make([]string, 0)
	for {
		user, err := GetUserName(bson.M{"_id": bson.M{"$gt": lastFullUpdate.LastId}}, "_id")

		if err != nil {
			if err.Error() == "not found" {
				fmt.Println("User data updated, restart, last id: ", lastFullUpdate.LastId)
				lastFullUpdate.LastId = 0
				lastFullUpdate.Save()
				continue
			} else {
				errorsLog = append(errorsLog, err.Error())
			}
		}
		logs = append(logs, fmt.Sprintf("%v=%v data update", user.Id, user.Name))
		profile, err := malpar.GetUserProfileDataByUserName(user.Name, 3)
		if err != nil {
			errorsLog = append(errorsLog, err.Error())
		}
		userData := UserData{user.Id, user.Name, profile.LastLogin, profile.Gender, profile.Birthday, profile.Joined, profile.Location}
		err = userData.Update()
		if err != nil {
			errorsLog = append(errorsLog, err.Error())
		}
		lastFullUpdate.LastId = user.Id
		lastFullUpdate.Save()

		if len(logs) > 100 {
			fmt.Println("User data updated ", logs)
			logs = make([]string, 0)
		}
		if len(errorsLog) > 100 {
			fmt.Println("User data update errors ", errorsLog)
			errorsLog = make([]string, 0)
		}
	}
}

func GetNewUsers() {
	lastUser, err := GetUserName(nil, "-_id")
	if err == mgo.ErrNotFound {
		fmt.Println("Last user not found")
		lastUser = UserName{0, "", time.Now()}
	} else {
		if err != nil {
			fmt.Println(err)
			return
		}
	}

	logs := make([]string, 0)
	for i := -CHECKUSERRANGE; i < CHECKUSERRANGE; i++ {
		userId := lastUser.Id + i
		username, err := malpar.GetUserNameById(userId, 3)
		if err != nil {
			fmt.Println("User update error ", err, userId)
		} else {
			logs = append(logs, fmt.Sprintf("%v=%v ", userId, username))
			user := UserName{userId, username, time.Now()}
			user.Save()
		}
		if len(logs) > 100 {
			fmt.Println("User added ", logs)
			logs = make([]string, 0)
		}
	}
}
