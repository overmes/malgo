package control

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"html/template"
	"net/http"
)

const (
	DATABASE = "control"
)

var homeTempl *template.Template

func serveHome(w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", 405)
		return
	}
	// TODO: remove
	//homeTempl = template.Must(template.ParseFiles("templates/home.html"))
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	homeTempl.Execute(w, r.Host)
}

type BaseJsonResponse struct {
	Status  string
	Message string
}

type DataJsonResponse struct {
	BaseJsonResponse
	Data interface{}
}

func GetErrorResponse(err error) interface{} {
	return BaseJsonResponse{"error", err.Error()}
}

func GetDataResponse(data interface{}) interface{} {
	return DataJsonResponse{BaseJsonResponse{"ok", ""}, data}
}

func FormatResponse(data interface{}, error error) interface{} {
	if error != nil {
		return GetErrorResponse(error)
	} else {
		return GetDataResponse(data)
	}
}

type ControlServer struct {
	Host      string
	MongoHost string
	Servers   []string
	Session   *mgo.Session
}

var controlServer *ControlServer

func (c *ControlServer) Start() {
	homeTempl = template.Must(template.ParseFiles("templates/home.html"))
	controlServer = c

	var session, mongoError = mgo.Dial(c.MongoHost)
	if mongoError != nil {
		panic(mongoError)
	}
	controlServer.Session = session
	defer session.Close()

	go NewResultCounter(c.Servers).Start()
	go StartUserUpdater()

	fmt.Println("start")

	fs := http.FileServer(http.Dir("static"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.HandleFunc("/api/result/", serveResult)
	http.HandleFunc("/api/user/", serveUser)
	http.HandleFunc("/", serveHome)

	err := http.ListenAndServe(c.Host, nil)
	if err != nil {
		panic(err)
	}
	fmt.Println("stop")
}
