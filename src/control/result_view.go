package control

import (
	"encoding/json"
	"errors"
	"fmt"
	"github.com/fatih/structs"
	"gopkg.in/mgo.v2/bson"
	"io/ioutil"
	"net/http"
)

func getResultId(url string) bson.ObjectId {
	return bson.ObjectIdHex(url[len("/api/result/"):])
}

func FillUserData(results []Result) ([]interface{}, error) {
	ids := make([]int32, 0)

	for i := range results {
		for j := range results[i].Pearson {
			current := results[i].Pearson[j]
			ids = append(ids, current.Id)
		}
	}

	userNames, err := FilterUserData(bson.M{"_id": bson.M{"$in": ids}}, "", 0)
	if err != nil {
		fmt.Println("Error while get user names", err)
		return nil, err
	}
	userNamesMap := make(map[int32]UserData, 0)
	for i := range userNames {
		userNamesMap[int32(userNames[i].Id)] = userNames[i]
	}

	filledResults := make([]interface{}, 0)
	for i := range results {
		resultsWithData := make([]interface{}, 0)
		for j := range results[i].Pearson {
			if user, ok := userNamesMap[results[i].Pearson[j].Id]; ok {
				pearsonMap := structs.Map(results[i].Pearson[j])
				pearsonMap["UserName"] = user.Name
				pearsonMap["LastOnline"] = user.LastOnline
				pearsonMap["Gender"] = user.Gender
				pearsonMap["Birthday"] = user.Birthday
				pearsonMap["Joined"] = user.Joined
				pearsonMap["Location"] = user.Location
				resultsWithData = append(resultsWithData, pearsonMap)
			}
		}
		resultMap := structs.Map(results[i])
		resultMap["Pearson"] = resultsWithData
		filledResults = append(filledResults, resultMap)
	}
	return filledResults, nil
}

func listResult(r *http.Request) (interface{}, error) {
	get := r.URL.Query()

	var err error
	var results []Result
	name, ok := get["username"]
	if ok {
		results, err = FilterResult(bson.M{"username": name[0]}, "-created", 100)
	} else {
		results, err = FilterResult(nil, "-created", 100)
	}
	if results == nil {
		results = make([]Result, 0)
	}
	filledResult, err := FillUserData(results)
	return filledResult, err
}

func createResult(r *http.Request) (interface{}, error) {
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if err != nil {
		return "", errors.New("Wrong data")
	}
	receivedResult := Result{}
	err = json.Unmarshal(body, &receivedResult)
	if err != nil {
		return nil, errors.New("Wrong data")
	}
	if receivedResult.UserName == "" {
		return nil, errors.New("User name required")
	}
	results, resErr := FilterResult(bson.M{"username": receivedResult.UserName}, "", 100)
	if resErr != nil {
		fmt.Println(resErr)
	}
	toSaveResult := NewResult()
	toSaveResult.UserName = receivedResult.UserName
	toSaveResult.Anime = receivedResult.Anime
	toSaveResult.Manga = receivedResult.Manga
	toSaveResult.Share = receivedResult.Share
	if len(results) > 20 {
		toSaveResult.Status = COMPLITED
		toSaveResult.Progress = 100
		toSaveResult.Errors = []string{"Maximum 20 stored results. Delete some of them."}
	}
	toSaveResult.Save()
	return toSaveResult, nil
}

func detailResult(r *http.Request) (interface{}, error) {
	resultId := getResultId(r.URL.Path)
	result, err := GetResult(bson.M{"_id": resultId}, "")
	if err != nil {
		return result, err
	}
	filledResults, err := FillUserData([]Result{result})
	return filledResults[0], err
}

func deleteResult(r *http.Request) (interface{}, error) {
	resultId := getResultId(r.URL.Path)
	return nil, RemoveResult(bson.M{"_id": resultId})
}

func serveResult(w http.ResponseWriter, r *http.Request) {
	var res interface{}
	var err error

	switch r.Method {
	case "GET":
		strId := r.URL.Path[len("/api/result/"):]
		if len(strId) == 0 {
			res, err = listResult(r)
		} else {
			res, err = detailResult(r)
		}
	case "POST":
		res, err = createResult(r)
	case "DELETE":
		res, err = deleteResult(r)

	}

	statusCode := http.StatusOK
	if err != nil {
		statusCode = http.StatusInternalServerError
	}
	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(FormatResponse(res, err))
}
