package control

import (
	"gopkg.in/mgo.v2/bson"
	"malpar"
	"time"
)

const (
	COMPLITED = "complited"
	COUNTING  = "counting"
	PENDING   = "pending"
	ERROR     = "error"

	USERNAMECOLLECTION = "username"
	RESULTCOLLECTION   = "result"
)

func Filter(collection string, res interface{}, filter bson.M, sort string, limit int) error {
	c := controlServer.Session.DB(DATABASE).C(collection)
	query := c.Find(filter)
	if sort != "" {
		query = query.Sort(sort)
	}
	if limit != 0 {
		query = query.Limit(limit)
	}
	err := query.All(res)
	return err
}

func Get(collection string, res interface{}, filter bson.M, sort string) error {
	c := controlServer.Session.DB(DATABASE).C(collection)
	query := c.Find(filter)
	if sort != "" {
		query = query.Sort(sort)
	}
	err := query.One(res)
	return err
}

type UserData struct {
	Id         int `bson:"_id"`
	Name       string
	LastOnline time.Time
	Gender     string
	Birthday   string
	Joined     string
	Location   string
}

func (u *UserData) Update() error {
	c := controlServer.Session.DB(DATABASE).C(USERNAMECOLLECTION)
	return c.Update(bson.M{"_id": u.Id}, u)
}

type UserName struct {
	Id         int `bson:"_id"`
	Name       string
	LastOnline time.Time
}

func FilterUserData(filter bson.M, sort string, limit int) ([]UserData, error) {
	var res []UserData
	return res, Filter(USERNAMECOLLECTION, interface{}(&res), filter, sort, limit)
}

func GetUserName(filter bson.M, sort string) (UserName, error) {
	res := UserName{}
	return res, Get(USERNAMECOLLECTION, interface{}(&res), filter, sort)
}

func (u *UserName) Save() error {
	c := controlServer.Session.DB(DATABASE).C(USERNAMECOLLECTION)
	count, err := c.FindId(u.Id).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return c.Insert(u)
	} else {
		return u.Update()
	}
}

func (u *UserName) Delete() error {
	c := controlServer.Session.DB(DATABASE).C(USERNAMECOLLECTION)
	return c.RemoveId(u.Id)
}

func (u *UserName) Update() error {
	c := controlServer.Session.DB(DATABASE).C(USERNAMECOLLECTION)
	return c.Update(bson.M{"_id": u.Id}, bson.M{"$set": u})
}

type Result struct {
	Id        bson.ObjectId `bson:"_id"`
	UserName  string
	Status    string
	Pearson   malpar.PearsonSlice
	Compare   int
	Errors    []string
	Created   time.Time
	Completed time.Time
	Manga     bool
	Anime     bool
	Share     int
	Progress  int
}

func NewResult() *Result {
	res := Result{Id: bson.NewObjectId(), Errors: make([]string, 0), Pearson: make(malpar.PearsonSlice, 0),
		Created: time.Now(), Status: PENDING}
	return &res
}

func FilterResult(filter bson.M, sort string, limit int) ([]Result, error) {
	var res []Result
	return res, Filter(RESULTCOLLECTION, interface{}(&res), filter, sort, limit)
}

func GetResult(filter bson.M, sort string) (Result, error) {
	res := Result{}
	return res, Get(RESULTCOLLECTION, interface{}(&res), filter, sort)
}

func RemoveResult(filter bson.M) error {
	c := controlServer.Session.DB(DATABASE).C(RESULTCOLLECTION)
	err := c.Remove(filter)
	return err
}

func (r *Result) Save() error {
	c := controlServer.Session.DB(DATABASE).C(RESULTCOLLECTION)
	return c.Insert(r)
}

func (r *Result) Update() error {
	c := controlServer.Session.DB(DATABASE).C(RESULTCOLLECTION)
	return c.Update(bson.M{"_id": r.Id}, r)
}
