package main

import (
	"os"
	"strconv"
	"worker"
)

func main() {
	host := os.Args[1]
	id, err := strconv.Atoi(os.Args[2])
	if err != nil {
		panic(err)
	}
	count, err := strconv.Atoi(os.Args[3])
	if err != nil {
		panic(err)
	}
	control := os.Args[4]
	postgres := os.Args[5]

	worker := worker.PearsonCountServer{Postgres: postgres, Host: host,
		CurrentId: id, ServersCount: count, Control: control}
	worker.Start()
}
