package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"github.com/syndtr/goleveldb/leveldb"
	"log"
	"math/rand"
	"strconv"
)

type Title struct {
	Id         int32
	Score      int8
	Type       int8
	StartDate  uint
	EndDate    uint
	LastUpdate uint
}

func writeRandomScores(db *leveldb.DB, count int) {
	for i := 0; i < count; i++ {
		userList := make([]Title, 0)
		for i := 0; i < 100; i++ {
			title := Title{int32(i), int8(rand.Float32() * 10), int8(rand.Float32() * 10),
				uint(rand.Float32() * 10000), uint(rand.Float32() * 10000), uint(rand.Float32() * 10000)}
			userList = append(userList, title)
		}

		//userList := malpar.UserList{1, "test", make([]malpar.AnimeTitle, 0), make([]malpar.MangaTitle, 0)}
		//for i := 0; i < 100; i++ {
		//	title := malpar.Title{uint(rand.Float32() * 10000), uint(rand.Float32() * 10000), int(rand.Float32() * 10000),
		//		"2007-" + strconv.Itoa(int(rand.Float32()*1000)), "2008-" + strconv.Itoa(int(rand.Float32()*1000))}
		//	userList.AnimeList = append(userList.AnimeList, malpar.AnimeTitle{title, uint(i)})
		//	userList.MangaList = append(userList.MangaList, malpar.MangaTitle{title, uint(i)})
		//}
		var userListBuffer bytes.Buffer
		enc := gob.NewEncoder(&userListBuffer)
		err := enc.Encode(userList)
		if err != nil {
			log.Fatal(err)
		}

		value := userListBuffer.Bytes()
		if i%1000 == 0 {
			fmt.Println(i, userList)
		}

		key := []byte(strconv.Itoa(i))
		err = db.Put(key, value, nil)
		if err != nil {
			log.Fatal(err)
		}

	}
}

func main() {
	db, err := leveldb.OpenFile("level.db", nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	writeRandomScores(db, 400*1000)

	iter := db.NewIterator(nil, nil)
	for iter.Next() {
		key := iter.Key()
		i, err := strconv.Atoi(string(key))
		if err != nil {
			log.Fatal(err)
		}
		if i%1000 == 0 {
			value := iter.Value()
			var userListBuffer bytes.Buffer
			userListBuffer.Write(value)
			dec := gob.NewDecoder(&userListBuffer)

			var userList []Title
			err = dec.Decode(&userList)
			if err != nil {
				log.Fatal("decode error 1:", err)
			}

			fmt.Println(i, userList)
		}

	}
	iter.Release()
	err = iter.Error()

}
