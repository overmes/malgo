package main

import (
	"fmt"
	"malpar"
	"time"
)

func main() {
	scoresCount := 0
	usersScores := make([]malpar.UserScoresSlice, 0)
	for i := 0; i < 2*1000*1000; i++ {
		userScores := make(malpar.UserScoresSlice, 0)
		for j := 0; j < 100; j++ {
			userScores = append(userScores, malpar.UserScore{Id: int32(i * j), Score: int8(i * j)})
			scoresCount++
		}
		usersScores = append(usersScores, userScores)
		fmt.Println(scoresCount)
	}
	fmt.Println(scoresCount)
	time.Sleep(100 * time.Second)
}
