package main

import (
	"control"
	"fmt"
	"os"
	"strings"
)

func main() {
	host := os.Args[1]
	mongoHost := os.Args[2]
	servers := strings.Split(os.Args[3], ",")

	control := control.ControlServer{Host: host, MongoHost: mongoHost,
		Servers: servers}
	startWithRecover(control, 0)
}

func startWithRecover(control control.ControlServer, count int) {
	defer func() {
		if r := recover(); r != nil {
			fmt.Printf("======== Recover %v ==========\n", count)
			fmt.Println(r)
			if count < 5 {
				startWithRecover(control, count+1)
			}
		}
	}()
	control.Start()
}
