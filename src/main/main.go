package main

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"github.com/boltdb/bolt"
	"log"
	"strconv"
)

var world = []byte("users")

type Title struct {
	Id    int32
	Score int8
	Date  uint
}

func main() {
	db, err := bolt.Open("my.db", 0644, nil)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	//userList := malpar.UserList{1, "test", make([]malpar.AnimeTitle, 0), make([]malpar.MangaTitle, 0)}
	//for i := 0; i < 100; i++ {
	//	title := malpar.Title{10, 10, 10, "", ""}
	//	userList.AnimeList = append(userList.AnimeList, malpar.AnimeTitle{title, uint(i)})
	//	userList.MangaList = append(userList.MangaList, malpar.MangaTitle{title, uint(i)})
	//}

	userList := make([]Title, 0)
	for i := 0; i < 201; i++ {
		userList = append(userList, Title{int32(i), 5, uint(i * 1000)})
	}

	var userListBuffer bytes.Buffer
	enc := gob.NewEncoder(&userListBuffer)
	err = enc.Encode(userList)
	if err != nil {
		log.Fatal(err)
	}

	value := userListBuffer.Bytes()
	fmt.Println(len(value))

	err = db.Update(func(tx *bolt.Tx) error {
		for i := 0; i < 200*1000; i++ {
			if i%1000 == 0 {
				fmt.Println(i)
			}

			bucket, err := tx.CreateBucketIfNotExists(world)
			if err != nil {
				return err
			}
			key := []byte(strconv.Itoa(i))
			err = bucket.Put(key, value)
			if err != nil {
				return err
			}

		}
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}

}
