package worker

import (
	"control"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"malpar"
	"net/http"
)

type ScoresMessage struct {
	Scores []malpar.UserList
}

//func (s *ScoresMessage) Save() error {
//	errorsBuf := bytes.Buffer{}
//	for i := range s.Scores {
//		if countServer.IsMyUser(s.Scores[i].UserId) {
//			err := ScoresFromList(s.Scores[i]).Save()
//			if err != nil {
//				fmt.Println("err", err)
//				errorsBuf.WriteString(err.Error())
//			}
//			countServer.PearsonCounter.UpdateScores(&s.Scores[i])
//		}
//	}
//	if errorsBuf.Len() > 0 {
//		return errors.New(errorsBuf.String())
//	} else {
//		return nil
//	}
//}

func serveScores(w http.ResponseWriter, r *http.Request) {
	var res interface{}
	var err error

	switch r.Method {
	case "GET":
	case "POST":
		var body []byte
		body, err = ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err != nil {
			res, err = "", errors.New("Wrong data")
		}
		scores := ScoresMessage{}
		err = json.Unmarshal(body, &scores)
		if err != nil {
			res, err = nil, errors.New("Wrong data")
		}
		//err = scores.Save()
		fmt.Println("Loaded user scores ", len(scores.Scores))
		res = len(scores.Scores)
	case "DELETE":

	}

	statusCode := http.StatusOK
	if err != nil {
		statusCode = http.StatusInternalServerError
	}
	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(control.FormatResponse(res, err))
}
