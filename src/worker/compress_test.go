package worker

import (
	"fmt"
	"malpar"
	"testing"
)

func TestScoresCompress(t *testing.T) {
	anime1 := malpar.AnimeTitle{malpar.Title{5, 10, 2422242123, "", ""}, 40123}
	anime2 := malpar.AnimeTitle{malpar.Title{6, 0, 2122242123, "", ""}, 90123}
	manga1 := malpar.MangaTitle{malpar.Title{1, 7, 2412242123, "", ""}, 100123}
	manga2 := malpar.MangaTitle{malpar.Title{2, 9, 2432242123, "", ""}, 10123}
	userList := malpar.UserList{10, "some", []malpar.AnimeTitle{
		anime1,
		anime2,
	}, []malpar.MangaTitle{
		manga1,
		manga2,
	}}

	scores := ScoresFromList(userList)
	answer := "{40123105,90123006,-100123071,-10123092}"
	if scores.IdScoreStat != answer {
		t.Error(scores.IdScoreStat, answer)
	}
	fmt.Println(scores.IdScoreStat)
	fmt.Println(StringIntArray(scores.IdScoreStat).ToArray())
	scoresSlice := scores.ToScoresSlice()
	if len(*scoresSlice) < 3 {
		t.Error(scoresSlice)
	}

	for _, v := range *scoresSlice {
		switch v.Id {
		case int32(anime1.Id):
			if v.Score != int8(anime1.Score) {
				t.Error(v, anime1)
			}
		case int32(-manga1.Id):
			if v.Score != int8(manga1.Score) {
				t.Error(v, manga1)
			}
		case int32(-manga2.Id):
			if v.Score != int8(manga2.Score) {
				t.Error(v, manga2)
			}
		}
	}

}
