package worker

import (
	"control"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	"io/ioutil"
	"malpar"
	"net/http"
	"time"
)

type ScoresUpdater struct {
	Result chan Scores
}

func NewScoresUpdater() *ScoresUpdater {
	return &ScoresUpdater{make(chan Scores, 100)}
}

func (s *ScoresUpdater) Start() {
	logs := make([]string, 0)
	for {
		lastScoresSlice, err := GetLastScores()
		if err != nil {
			fmt.Println("Scores update error", err)
			time.Sleep(5 * time.Second)
		}
		toSave := make([]Scores, 0)
		for _, lastScores := range lastScoresSlice {
			update, err := malpar.GetUserScoresByName(lastScores.UserName, 3)
			if err != nil {
				strErr := fmt.Sprintf("Update score error %v, %v, %v", lastScores.UserName, lastScores.Id, err)
				fmt.Println(strErr)
				toSave = append(toSave, lastScores)
				time.Sleep(5 * time.Second)
			} else {
				toSave = append(toSave, *ScoresFromList(update))
				countServer.PearsonCounter.UpdateScores(&update)
				logs = append(logs, fmt.Sprintf("%v %v ", lastScores.UserName, len(update.AnimeList)+len(update.MangaList)))
			}
		}
		db, err := sqlx.Connect("postgres", countServer.Postgres)
		if err != nil {
			fmt.Println("Can't connect to psql")
			continue
		}
		for _, score := range toSave {
			score.Save(db)
		}
		db.Close()
		fmt.Println("User updated", logs)
		logs = make([]string, 0)
	}
}

type UserJsonResponse struct {
	control.BaseJsonResponse
	Data []control.UserName
}

func (s *ScoresUpdater) UpdateUserNames() {
	limit := 10000
	errorCount := 0
	for {
		hasData := true
		offset := 0
		for hasData {
			userNames, err := s.GetUserNames(offset, limit)
			fmt.Println("User names update ", len(userNames), offset)
			if err != nil {
				fmt.Println("User names update error ", err)
				errorCount++
			} else {
				hasData = len(userNames) != 0
				offset += limit
				SaveUserNamesUpdate(userNames)
			}
			if errorCount > 100 {
				break
			}
			time.Sleep(30 * time.Second)
		}
		time.Sleep(48 * time.Hour)
	}

}

func SaveUserNamesUpdate(userNames []control.UserName) {
	db, err := sqlx.Connect("postgres", countServer.Postgres)
	if err != nil {
		fmt.Println("Can't connect to psql")
		return
	}
	defer db.Close()
	for i := range userNames {
		if countServer.IsMyUser(userNames[i].Id) {
			scores := EmptyScores(userNames[i].Id, userNames[i].Name, userNames[i].LastOnline)
			err = scores.UpdateUserName(db)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
}

func (s *ScoresUpdater) GetUserNames(offset int, limit int) ([]control.UserName, error) {
	client := http.Client{Timeout: 60 * time.Second}
	data := UserJsonResponse{}

	resp, err := client.Get(countServer.Control + fmt.Sprintf("?offset=%v&limit=%v", offset, limit))
	if err != nil {
		return data.Data, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return data.Data, errors.New("Status code: " + string(resp.StatusCode))
	}
	var body []byte
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return data.Data, err
	}

	err = json.Unmarshal(body, &data)
	if err != nil {
		return data.Data, err
	}
	if data.Status != "ok" {
		return data.Data, errors.New(data.Message)
	}
	if err != nil {
		return data.Data, err
	}
	return data.Data, nil
}
