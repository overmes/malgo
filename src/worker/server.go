package worker

import (
	"fmt"
	_ "github.com/lib/pq"
	"net/http"
)

type PearsonCountServer struct {
	Postgres       string
	Host           string
	CurrentId      int
	ServersCount   int
	Control        string
	PearsonCounter *PearsonCounter
	ScoresUpdater  *ScoresUpdater
}

var countServer *PearsonCountServer

func (p *PearsonCountServer) IsMyUser(userId int) bool {
	if userId != 0 && (userId%p.ServersCount) == p.CurrentId-1 {
		return true
	} else {
		return false
	}
}

func (p *PearsonCountServer) Start() {
	countServer = p

	p.PearsonCounter = NewPearsonCounter()
	p.ScoresUpdater = NewScoresUpdater()

	p.PearsonCounter.Prepare()
	go p.PearsonCounter.Start()
	go p.ScoresUpdater.UpdateUserNames()
	go p.ScoresUpdater.Start()

	fmt.Println("start")
	http.HandleFunc("/api/pearson/", servePearson)
	http.HandleFunc("/api/score/", serveScores)

	err := http.ListenAndServe(p.Host, nil)
	if err != nil {
		panic(err)
	}
	fmt.Println("stop")
}
