package worker

import (
	"bytes"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"malpar"
	"strconv"
	"strings"
	"time"
)

type StringIntArray string

func (s StringIntArray) ToArray() []int {
	split := strings.Split(string(s[1:len(s)-1]), ",")
	if split[0] != "" {
		res := make([]int, len(split))
		for i := range split {
			var err error
			res[i], err = strconv.Atoi(split[i])
			if err != nil {
				fmt.Println(err)
			}
		}
		return res
	} else {
		return []int{}
	}

}

type BuffersSlice []bytes.Buffer

func (b BuffersSlice) WriteSameChar(s string) {
	for i := range b {
		b[i].WriteString(s)
	}
}

func (b BuffersSlice) WriteChars(s ...string) {
	for i := range b {
		b[i].WriteString(s[i])
	}
}

type Scores struct {
	Id          int       `db:"id"`
	UserName    string    `db:"username"`
	IdScoreStat string    `db:"idscorestat"`
	Dates       string    `db:"dates"`
	LastUpdate  time.Time `db:"lastupdate"`
	LastOnline  time.Time `db:"lastonline"`
}

func CompressScore(id, score, status int) int {
	sign := 1
	if id < 0 {
		sign = -1
	}
	return id*1000 + sign*score*10 + sign*status
}

func DecompressIdScoreStat(val int) (int, int, int) {
	sigh := 1
	if val < 0 {
		sigh = -1
	}
	status := sigh * val % 10
	val = val / 10
	score := sigh * val % 100
	val = val / 100
	return val, score, status
}

func EmptyScores(id int, name string, lastOnline time.Time) *Scores {
	return &Scores{id, name, "{}", "{}", time.Now(), lastOnline}
}

func GetLastScores() ([]Scores, error) {
	query := fmt.Sprintf("SELECT * FROM scores WHERE id %% %v = %v ORDER BY lastupdate LIMIT 100", countServer.ServersCount, countServer.CurrentId-1)
	db, err := sqlx.Connect("postgres", countServer.Postgres)
	if err != nil {
		fmt.Println("can't connect to psql server")
	}
	defer db.Close()
	scores := []Scores{}
	err = db.Select(&scores, query)
	return scores, err
}

func (s *Scores) ToScoresSlice() *malpar.UserScoresSlice {
	result := malpar.UserScoresSlice{}

	idScoreStat := StringIntArray(s.IdScoreStat).ToArray()
	for i := range idScoreStat {
		item, score, _ := DecompressIdScoreStat(idScoreStat[i])
		if score > 0 {
			result = append(result, malpar.UserScore{int32(item), int8(score)})
		}
	}
	return &result
}

func (s *Scores) Exist(db *sqlx.DB) (bool, error) {
	res, err := db.Exec("SELECT username FROM scores WHERE username=$1", s.UserName)
	if err != nil {
		return false, err
	}
	var rows int64
	rows, err = res.RowsAffected()
	if err != nil {
		return false, err
	}
	if rows > 0 {
		return true, err
	} else {
		return false, err
	}
}

func (s *Scores) Insert(db *sqlx.DB) error {
	q := "INSERT INTO scores (id, username, idscorestat, dates, lastupdate, lastonline) VALUES ($1, $2, $3, $4, $5, $6)"
	_, err := db.Exec(q, s.Id, s.UserName, s.IdScoreStat, s.Dates, s.LastUpdate, s.LastOnline)
	return err
}

func (s *Scores) Save(db *sqlx.DB) error {
	ok, err := s.Exist(db)
	if err != nil {
		return err
	}

	s.LastUpdate = time.Now()
	if ok {
		_, err = db.Exec("UPDATE scores SET idscorestat=$2, dates=$3, lastupdate=$4, id=$5 WHERE username=$1",
			s.UserName, s.IdScoreStat, s.Dates, s.LastUpdate, s.Id)
	} else {
		err = s.Insert(db)
	}
	return err

}

func (s *Scores) UpdateUserName(db *sqlx.DB) error {
	ok, err := s.Exist(db)
	if err != nil {
		return err
	}

	s.LastUpdate = time.Now()
	if ok {
		withOnline := "UPDATE scores SET username=$1, lastupdate=$2, lastonline=$3 WHERE username=$1"
		_, err = db.Exec(withOnline, s.UserName, s.LastUpdate, s.LastOnline)
	} else {
		err = s.Insert(db)
	}
	return err

}

func ScoresFromList(list malpar.UserList) *Scores {
	Buffers := make(BuffersSlice, 2)

	Buffers.WriteSameChar("{")

	for i := range list.AnimeList {
		score := list.AnimeList[i]
		if i != 0 {
			Buffers.WriteSameChar(",")
		}
		idScoreStat := CompressScore(int(score.Id), int(score.Score), int(score.Status))
		Buffers.WriteChars(strconv.Itoa(idScoreStat), strconv.Itoa(int(score.LastUpdate)))
	}
	if len(list.AnimeList) > 0 && len(list.MangaList) > 0 {
		Buffers.WriteSameChar(",")
	}
	for i := range list.MangaList {
		score := list.MangaList[i]
		if i != 0 {
			Buffers.WriteSameChar(",")
		}
		idScoreStat := CompressScore(int(-score.Id), int(score.Score), int(score.Status))
		Buffers.WriteChars(strconv.Itoa(idScoreStat), strconv.Itoa(int(score.LastUpdate)))
	}

	Buffers.WriteSameChar("}")

	result := Scores{list.UserId, list.UserName, Buffers[0].String(),
		Buffers[1].String(), time.Now(), time.Now()}
	return &result
}
