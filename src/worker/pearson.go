package worker

import (
	"control"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"malpar"
	"net/http"
	"utils"
)

func servePearson(w http.ResponseWriter, r *http.Request) {
	var res interface{}
	var err error

	switch r.Method {
	case "GET":
	case "POST":
		var body []byte
		body, err = ioutil.ReadAll(r.Body)
		r.Body.Close()
		if err != nil {
			res, err = "", errors.New("Wrong data")
		}
		scores := control.PearsonRequest{}
		err = json.Unmarshal(body, &scores)
		if err != nil {
			res, err = nil, errors.New("Wrong data")
		}
		//if countServer.IsMyUser(scores.UserList.UserId) {
		//	saveErr := ScoresFromList(scores.UserList).Save()
		//	if saveErr != nil {
		//		fmt.Println("Save request user error", saveErr)
		//	}
		//}
		task := RequestChannel{scores, make(chan malpar.PearsonSlice, 1)}

		select {
		case countServer.PearsonCounter.RequestChan <- task:
			pearson := <-task.Response
			res = control.ResponseData{pearson[:utils.Min(len(pearson), 100)], len(pearson)}
			fmt.Println("Result: ", pearson[:utils.Min(len(pearson), 10)])
		default:
			msg := "Too much queries"
			err = errors.New(msg)
			fmt.Println(msg)
		}

	case "DELETE":

	}

	statusCode := http.StatusOK
	if err != nil {
		statusCode = http.StatusInternalServerError
	}
	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	json.NewEncoder(w).Encode(control.FormatResponse(res, err))
}
