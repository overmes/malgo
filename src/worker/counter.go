package worker

import (
	"control"
	"fmt"
	"github.com/jmoiron/sqlx"
	"malpar"
)

type RequestChannel struct {
	control.PearsonRequest
	Response chan malpar.PearsonSlice
}

type PearsonCounter struct {
	Pearson     *malpar.Pearson
	RequestChan chan RequestChannel
	UpdateChan  chan malpar.UserList
}

func NewPearsonCounter() *PearsonCounter {
	return &PearsonCounter{Pearson: malpar.NewPearson(), RequestChan: make(chan RequestChannel, 10), UpdateChan: make(chan malpar.UserList, 1000)}
}

func (p *PearsonCounter) UpdateScores(list *malpar.UserList) {
	if countServer.IsMyUser(list.UserId) {
		p.UpdateChan <- *list
	}
}

func (p *PearsonCounter) Prepare() {
	score := Scores{}
	db, err := sqlx.Connect("postgres", countServer.Postgres)
	if err != nil {
		panic(err)
	}
	defer db.Close()
	rows, err := db.Queryx(fmt.Sprintf("SELECT * FROM scores WHERE id %% %v = %v", countServer.ServersCount, countServer.CurrentId-1))
	if err != nil {
		panic(err)
	}
	for rows.Next() {
		err := rows.StructScan(&score)
		if err != nil {
			fmt.Println(err)
		} else {
			scoresSlice := score.ToScoresSlice()
			if len(*scoresSlice) > 0 && countServer.IsMyUser(score.Id) {
				p.Pearson.UpdateUserSlices(score.Id, *scoresSlice)
			}
		}
	}
	fmt.Println("Loaded ", len(p.Pearson.Scores), " users")

}

func (p *PearsonCounter) Start() {
	//var list malpar.UserList
	for {
		select {
		case list := <-p.UpdateChan:
			if countServer.IsMyUser(list.UserId) {
				p.Pearson.UpdateUserList(list)
			}
		case req := <-p.RequestChan:
			pearson := p.Pearson.Count(req.UserList, req.Share, req.Anime, req.Manga, countServer.IsMyUser(req.UserList.UserId))
			req.Response <- pearson
		}
	}
}
