CREATE TABLE scores (
  id       INT,
  username TEXT PRIMARY KEY,
  idscorestat   INT [],
  dates    INT [],
  lastupdate TIMESTAMP
);
CREATE INDEX lastupdate_idx ON scores (lastupdate);
ALTER TABLE scores ADD COLUMN lastonline TIMESTAMP DEFAULT to_timestamp('2000', 'YYYY');
CREATE INDEX lastonline_idx ON scores (lastonline);
CREATE TABLE errors (
  id SERIAL PRIMARY KEY NOT NULL,
  error TEXT

);